from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from datetime import datetime


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_AccountVO(ch, method, properties, body): # Declare a function to update the AccountVO object (ch, method, properties, body)

    content = json.loads(body)#   content = load the json in body
    first_name = content["first_name"]#   first_name = content["first_name"]
    last_name = content["last_name"]#   last_name = content["last_name"]
    email = content["email"]#   email = content["email"]
    is_active = content["is_active"]#   is_active = content["is_active"]
    updated_string = content["updated"]#   updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)#   updated = convert updated_string from ISO string to datetime
    if is_active: #   if is_active:
        AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_active=is_active,
            updated=updated,
        )    #       Use the update_or_create method of the AccountVO.objects QuerySet
#           to update or create the AccountVO object
    else: #   otherwise:
        avo = AccountVO.objects.get(email=email)#       Delete the AccountVO object with the specified email, if it exists
        if avo:
            avo.delete()

# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
while True:# infinite loop
    try:    #   try
        parameters = pika.ConnectionParameters(host='rabbitmq')    #       create the pika connection parameters
        connection = pika.BlockingConnection(parameters)    #       create a blocking connection with the parameters
        channel = connection.channel()  #       open a channel
        channel.exchange_declare(exchange='account_info', exchange_type='fanout')#       declare a fanout exchange named "account_info"
        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange='account_info', queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_AccountVO,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        time.sleep(2.0)
    except:
        print("other error")
        time.sleep(2.0)
#       declare a randomly-named queue
#       get the queue name of the randomly-named queue
#       bind the queue to the "account_info" exchange
#       do a basic_consume for the queue name that calls
#           function above
#       tell the channel to start consuming
#   except AMQPConnectionError
#       print that it could not connect to RabbitMQ
#       have it sleep for a couple of seconds
